#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

union compactFloat {
	float value;
	unsigned char bytes[sizeof(float)];
};

void printCompactFloat(float value) {
	union compactFloat myFloat;
	myFloat.value = value;

	int bits[32];
	int* p;

	p = &myFloat.value;
	for (int i = sizeof(int) * 8 - 1, j = 0; i >= 0; i--, j++)
		bits[j] = (*p) >> i & 1;

	printf("\n1) �������� �������: ");
	for (int i = 0; i < 32; i++)
		printf("%d", bits[i]);
	printf("\n");


	printf("2) �������� ���������: ");
	for (int i = 8; i <= 32; i += 8) {
		int j = 1, k = 0;
		for (int n = i - 1; i - 8 <= n; n--, j *= 2)
			if (bits[n])
				k += j;
		printf("%02x ", k); 
	}
	printf("\n");

	printf("3) ����: %d\n", bits[0]);

	printf("4) �������: ");
	for (int i = 9; i < 32; i++)
		printf("%d", bits[i]);
	printf("\n");

	printf("5) C����� ��������: ");
	for (int i = 1; i < 9; i++)
		printf("%d", bits[i]);
	printf("\n");

	printf("6) ����� ���'��: %lu ����\n", sizeof(myFloat));
}

int main() {
	system("chcp 1251"); // ������� � ������ �� ��������
	system("cls"); // ������� ���� ������

	float value;
	printf("������ ��������: "); scanf("%f", &value);
	printCompactFloat(value);

	return 0;
}