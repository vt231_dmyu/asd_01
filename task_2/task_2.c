#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

union compactShort {
	signed short value;
	unsigned char bytes[sizeof(short)];
};

void printShortByUnion(signed short var) {
	union compactShort myShort;
	myShort.value = var;

	printf("����: %d\n", (myShort.value > 0) ? 1 : -1);
	printf("��������: %d\n", abs(myShort.value));
}

int getSign(signed short var) {
	const int BITS_NUMBER = 8;
	return (var != 0) | var >> (sizeof(short) * BITS_NUMBER - 1);
}

void printShortByLogicOperators(signed short var) {
	printf("����: %d\n", getSign(var));
	printf("��������: %d\n", abs(var));
}

int main() {
	system("chcp 1251"); // ������� � ������ �� ��������
	system("cls"); // ������� ���� ������

	signed short var;
	printf("������ ������������ ��������: ");
	scanf("%hd", &var);
	printShortByUnion(var);
	printShortByLogicOperators(var);

	return 0;
}