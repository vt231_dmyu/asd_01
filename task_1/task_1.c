﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

	struct dateTime {
		unsigned short nYear;
		unsigned short nMonth : 4;
		unsigned short nDay : 5;
		unsigned short nHour : 5;
		unsigned short nMin : 6;
		unsigned short nSec : 6;
	};

int main() {
	system("chcp 1251"); // Перехід в консолі на кирилицю
	system("cls"); // Очищаємо вікно консолі

	struct dateTime varOne = {2024, 3, 19, 20, 25, 30};
	struct tm varTwo;
	printf("Обсяг пам'яті, яку займає змінна типу dateTime = %d\n", sizeof(varOne));
	printf("Обсяг пам'яті, яку займає змінна типу tm = %d\n", sizeof(varTwo));
	printf("%d.%d.%d %d:%d:%d", varOne.nDay, varOne.nMonth, varOne.nYear, varOne.nHour, varOne.nMin, varOne.nSec);

	return 0;
}
